# README #

### What is this repository for? ###

** Quick summary

Zu entwickeln ist ein REST Web Service mit Spring Boot und dem zugeh�rigen Test mit JUnit. Die Idee hinter dem Service ist die Berechnung von Wechselgeld beim Wareneinkauf.
Dabei soll die passende St�ckelung f�r den Einkauf ermittelt und dem Kassierer hiermit geholfen werden dem Kunden die passenden Banknoten/M�nzen auszugeben.

Der Service soll zwei Parameter erwarten. Der erste Parameter beinhaltet die tats�chlich angefallenen Kosten f�r den Einkauf (Gesamtkosten).
Der zweite Parameter beinhaltet den erhaltenen Betrag vom Kunden. Der Service muss das R�ckgeld passend berechnen und dabei die kleinste m�gliche St�ckelung f�r den Einkauf ermitteln.
Als Response sollte der Service die auszugebenen Banknoten und oder M�nzen zur�ckliefern in der entsprechenden Anzahl.


Beispiel:
1) Kunde kauft f�r 15,00� ein bezahlt mit 20,00�, Wechselgeld = 5,00� - Service liefert 5,00�.

2) Kunde kauft f�r 70,00� ein bezahlt mit 100,00�, Wechselgeld = 30,00� - Service liefert 20,00� + 10,00�.

3) Kunde kauft f�r 3,50� ein bezahlt mit 5,00�, Wechselgeld = 1,50� - Service liefert 1,00� + 0,50�.



Folgende Punkte sind bei der Umsetzung zu beachten:

1)         Projekt Aufbau und Struktur (Maven Projekt).

2)         Design Pattern (REST Controllers/Services, MVC).

3)         HTTP Response Code.

4)         JSON R�ckmeldung.

5)         JUnit Test und/oder Integrationstest REST Web Service.


** Version
1.0.0

### How do I get set up? ###

* Summary of set up

* Configuration - Port 8080

* Dependencies:
-spring-boot-starter-web,
-spring-boot-starter-test,
-springfox-swagger2,
-springfox-swagger-ui,
-lombok

* Database configuration   - keine DB eigesetzt

* How to run tests  - JUnut Test werden �ber Maven Goal test gestartet

* Deployment instructions  - eine Docker File wurde definiert. Jenkind bzw. GitLab CI Konfiguration fehlen


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines
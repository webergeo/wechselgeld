package com.bankverlag.wechselgeld;


import java.math.BigDecimal;

public enum Nennwert {
    EURO_500(500.0),
    EURO_200(200.0),
    EURO_100(100.0),
    EURO_50(50.0),
    EURO_20(20.0),
    EURO_10(10.0),
    EURO_5(5.0),
    EURO_2(2.0),
    EURO_1(1.0),
    CENT_50(0.50),
    CENT_10(0.10),
    CENT_5(0.05),
    CENT_2(0.02),
    CENT_1(0.01);


    public BigDecimal getValue() {
        return value;
    }

    private BigDecimal value;

    Nennwert(double value) {
        this.value = new BigDecimal(value);
    }


    @Override
    public String toString() {
        switch (this) {
            case CENT_1:
                return "Münze 1 Cent";
            case CENT_2:
                return "Münze 2 Cent";
            case CENT_5:
                return "Münze 5 Cent";
            case CENT_10:
                return "Münze 10 Cent";
            case CENT_50:
                return "Münze 50 Cent";
            case EURO_1:
                return "Münze 1 Euro";
            case EURO_2:
                return "Münze 2 Euro";
            case EURO_5:
                return "Banknote 5 Euro";
            case EURO_10:
                return "Banknote 10 Euro";
            case EURO_20:
                return "Banknote 20 Euro";
            case EURO_50:
                return "Banknote 50 Euro";
            case EURO_100:
                return "Banknote 100 Euro";
            case EURO_200:
                return "200 Euro";
            case EURO_500:
                return "500 Euro";
            default:
                return "undefiniertes Wert [" + this + "]";
        }
    }
}
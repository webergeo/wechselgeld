package com.bankverlag.wechselgeld;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.Map;

@RestController
@RequestMapping(path = "/wechselgeld")
public class Controller {


    public final static String REFERENCE_VARIABLE_GESAMTKOSTEN = "gesamtKosten";
    public final static String REFERENCE_VARIABLE_KUNDENBETRAG = "kundenBetrag";
    public final static String REFERENCE_SUBPATH = "/{" + REFERENCE_VARIABLE_GESAMTKOSTEN + "}/{" + REFERENCE_VARIABLE_KUNDENBETRAG + "}";


    private final WechselgeldService service;

    public Controller(final WechselgeldService service) {
        this.service = service;
    }

    @GetMapping(REFERENCE_SUBPATH)
    @ResponseStatus(HttpStatus.OK)
    public Map<Nennwert, Integer> calculate(@PathVariable(REFERENCE_VARIABLE_GESAMTKOSTEN) final BigDecimal gesamtKosten, @PathVariable(REFERENCE_VARIABLE_KUNDENBETRAG) final BigDecimal kundenBetrag) {
        return service.calculate(gesamtKosten, kundenBetrag);
    }
}

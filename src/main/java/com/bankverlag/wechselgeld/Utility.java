package com.bankverlag.wechselgeld;

import java.math.BigDecimal;

class Utility {

    private final static Utility instance = new Utility();

    private Utility() {
        super();
    }

    public static Utility getInstance() {
        return Utility.instance;
    }

    boolean checkInputParameter(BigDecimal gesamtKosten, BigDecimal kundenBetrag) {
        if (gesamtKosten == null)
            throw new IllegalArgumentException("Parameter gesamtKosten beim Aufruf der Methode WechselgeldService.calculate wurde nicht definiert!");

        if (kundenBetrag == null)
            throw new IllegalArgumentException("Parameter kundenBetrag beim Aufruf der Methode WechselgeldService.calculate wurde nicht definiert!");

        if (gesamtKosten.compareTo(BigDecimal.ZERO) <= 0)
            throw new IllegalArgumentException("Ungueltiger Parameter gesamtKosten [" + gesamtKosten + "] beim Aufruf der Methode WechselgeldService.calculate!");

        return gesamtKosten.compareTo(kundenBetrag) < 0;
    }
}

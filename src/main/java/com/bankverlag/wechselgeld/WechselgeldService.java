package com.bankverlag.wechselgeld;

import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.Map;

@Service
public class WechselgeldService {

    Map<Nennwert, Integer> calculate(final BigDecimal gesamtKosten, final BigDecimal kundenBetrag) {

        if (!Utility.getInstance().checkInputParameter(gesamtKosten, kundenBetrag)) return null;

        final Map<Nennwert, Integer> result = new HashMap<>();

        BigDecimal currentRest = kundenBetrag.subtract(gesamtKosten);


        for (Nennwert wert : Nennwert.values()) {

            final BigDecimal value = wert.getValue().setScale(2, RoundingMode.HALF_EVEN);

            final int valueInt = currentRest.divideToIntegralValue(value).intValue();

            if (valueInt > 0)
                result.put(wert, valueInt);

            currentRest = currentRest.remainder(value);
        }
        return result;
    }


}

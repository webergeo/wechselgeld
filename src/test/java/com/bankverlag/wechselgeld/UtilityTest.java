package com.bankverlag.wechselgeld;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

class UtilityTest {

    @Test
    void checkInputParameter() {

        try {
            Utility.getInstance().checkInputParameter(null, new BigDecimal("1"));
            fail();
        } catch (IllegalArgumentException e) {
            assertNotNull(e.getMessage());
        }

        try {
            Utility.getInstance().checkInputParameter(new BigDecimal("1"), null);
            fail();
        } catch (IllegalArgumentException e) {
            assertNotNull(e.getMessage());
        }

        assertTrue(Utility.getInstance().checkInputParameter(new BigDecimal("1"), new BigDecimal("2")));

        assertFalse(Utility.getInstance().checkInputParameter(new BigDecimal("2"), new BigDecimal("1")));

        assertFalse(Utility.getInstance().checkInputParameter(new BigDecimal("2"), new BigDecimal("2")));

        try {
            Utility.getInstance().checkInputParameter(new BigDecimal("1").negate(), new BigDecimal("1"));
            fail();
        } catch (IllegalArgumentException e) {
            assertNotNull(e.getMessage());
        }
    }
}
package com.bankverlag.wechselgeld;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class WechselgeldServiceTest {

    @Autowired
    private WechselgeldService service;

    @Test
    void calculate() {


        //1) Kunde kauft fuer 15,00€ ein bezahlt mit 20,00€, Wechselgeld = 5,00€ - Service liefert 5,00€


        BigDecimal gesamtKosten = new BigDecimal("15.00");
        BigDecimal kundenBetrag = new BigDecimal("20.00");
        Map<Nennwert, Integer> expected = new HashMap<>();
        expected.put(Nennwert.EURO_5, 1);
        assertEquals(expected, service.calculate(gesamtKosten, kundenBetrag));

        // 2) Kunde kauft fuer 70,00€ ein bezahlt mit 100,00€, Wechselgeld = 30,00€ - Service liefert 20,00€ + 10,00€
        gesamtKosten = new BigDecimal("70.00");
        kundenBetrag = new BigDecimal("100.00");
        expected = new HashMap<>();
        expected.put(Nennwert.EURO_10, 1);
        expected.put(Nennwert.EURO_20, 1);
        assertEquals(expected, service.calculate(gesamtKosten, kundenBetrag));

        //3) Kunde kauft fuer 3,50€ ein bezahlt mit 5,00€, Wechselgeld = 1,50€ - Service liefert 1,00€ + 0,50€
        gesamtKosten = new BigDecimal("3.50");
        kundenBetrag = new BigDecimal("5.00");
        expected = new HashMap<>();
        expected.put(Nennwert.EURO_1, 1);
        expected.put(Nennwert.CENT_50, 1);
        assertEquals(expected, service.calculate(gesamtKosten, kundenBetrag));

    }
}
FROM openjdk:11-jre-slim

COPY target/wechselgeld-1.0.0.jar /app/wechselgeld-1.0.0.jar

ENTRYPOINT exec java $JAVA_OPTS -jar app/wechselgeld-1.0.0.jar

EXPOSE 8080